<?php
	global $post;
	$saved_meta = get_post_meta( $post->ID, 'carousel_meta', true );
?>
<table class="wp-list-table widefat fixed striped posts">
	<tr>
		<td>Border Width</td>
		<td>
			<input type="text" name="car[border_width]" class="widefat" value="<?php echo (isset($saved_meta['border_width'])) ? $saved_meta['border_width'] : '8px' ; ?>">
		</td>
		<td>
			<p class="description">Provide width for border eg 2px, leave blank to disable border</p>
		</td>
	</tr>
	<tr>
		<td>Border Color</td>
		<td>
			<input type="text" name="car[border_color]" class="widefat colorpicker" value="<?php echo (isset($saved_meta['border_color'])) ? $saved_meta['border_color'] : '#FFF' ; ?>">
		</td>
		<td>
			<p class="description">Choose border color for carousel items</p>
		</td>
	</tr>
	<tr>
		<td>Arrows Color</td>
		<td>
			<input type="text" name="car[arrow_color]" class="widefat colorpicker" value="<?php echo (isset($saved_meta['arrow_color'])) ? $saved_meta['arrow_color'] : '#000' ; ?>">
		</td>
		<td>
			<p class="description">Choose arrows color</p>
		</td>
	</tr>
	<tr>
		<td>Title Background Color</td>
		<td>
			<input type="text" name="car[title_bg]" class="widefat colorpicker" value="<?php echo (isset($saved_meta['title_bg'])) ? $saved_meta['title_bg'] : '' ; ?>">
		</td>
		<td>
			<p class="description">Choose Title Background Color, Leave blank for default</p>
		</td>
	</tr>
	<tr>
		<td>Title Color</td>
		<td>
			<input type="text" name="car[title_color]" class="widefat colorpicker" value="<?php echo (isset($saved_meta['title_color'])) ? $saved_meta['title_color'] : '' ; ?>">
		</td>
		<td>
			<p class="description">Choose Title text color, Leave blank for default</p>
		</td>
	</tr>
	<tr>
		<td>Description Color</td>
		<td>
			<input type="text" name="car[desc_color]" class="widefat colorpicker" value="<?php echo (isset($saved_meta['desc_color'])) ? $saved_meta['desc_color'] : '' ; ?>">
		</td>
		<td>
			<p class="description">Choose description text color, Leave blank for default</p>
		</td>
	</tr>
	<tr>
		<td>Shadow</td>
		<td>
			<input type="text" name="car[shadow]" class="widefat" value="<?php echo (isset($saved_meta['shadow'])) ? $saved_meta['shadow'] : '1px 1px 3px rgba(0, 0, 0, 0.3)' ; ?>">
		</td>
		<td>
			<p class="description">Provide Shadow, <a href="http://www.cssmatic.com/box-shadow" target="_blank">Generate Shadow</a></p>
		</td>
	</tr>
	<tr>
		<td>Custom CSS</td>
		<td>
			<textarea name="car[custom_css]" class="widefat"><?php echo (isset($saved_meta['custom_css'])) ? stripcslashes($saved_meta['custom_css']) : '' ; ?></textarea>
		</td>
		<td>
			<p class="description">Paste custom css code here</p>
		</td>
	</tr>
</table>