<p class="description">Following are pro features. You can unlock them after purchasing <a target="_blank" href="https://webcodingplace.com/responsive-posts-carousel-wordpress-plugin/">pro version</a>. Thanks</p>
<table class="wp-list-table widefat fixed striped posts">
	<tr>
		<td>Title Font Family</td>
		<td>
			<input name="" type="text" disabled>
		</td>
		<td>
			<p class="description">Provide font family for title eg: <b>Roboto</b>.</p>
		</td>
	</tr>
	<tr>
		<td>Title Font Size</td>
		<td>
			<input name="" type="text" disabled>
		</td>
		<td>
			<p class="description">Provide font size for title eg: <b>20px</b>.</p>
		</td>
	</tr>
	<tr>
		<td>Description Font Family</td>
		<td>
			<input name="" type="text" disabled>
		</td>
		<td>
			<p class="description">Provide font family for description eg: <b>Cursive</b>.</p>
		</td>
	</tr>
	<tr>
		<td>Description Font Size</td>
		<td>
			<input name="" type="text" disabled>
		</td>
		<td>
			<p class="description">Provide font size for description eg: <b>16px</b>.</p>
		</td>
	</tr>
</table>