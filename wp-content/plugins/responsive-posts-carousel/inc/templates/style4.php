<div class="mega-post-carousel1">
	<div class="mega-post-image">
		<a href="<?php the_permalink(); ?>">
			<?php do_action( 'wcp_carousel_thumb_url', get_the_id(), $images_size ); ?>
		</a>

		<span class="mega-comment-box">
			<span class="mega-post-comment">
				<?php
					$comments = wp_count_comments(get_the_id());
					echo $comments->total_comments;
				?>
			</span>
		</span>
	</div>

	<div class="mega-post-category">
	<?php $categories = get_the_category();
		$limit = 1;
		$separator = ' ';
		$output = '';
		if ( ! empty( $categories ) ) {
		    foreach( $categories as $category ) {
		    	if ($limit < 4) {
		        	$output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
		        	$limit++;
		    	}
		    }
		    echo trim( $output, $separator );
		} ?>
	</div>

	<h3 class="mega-post-title">
		<a href="<?php the_permalink(); ?>">
			<?php do_action( 'wcp_carousel_title', $carousel_settings['title'], get_the_id() ); ?>
		</a>
	</h3>

	<span class="mega-post-meta wcp-disable-post-meta">
		<i class="fa fa-user"></i>
		<?php the_author_posts_link(); ?>
	</span>
	<span class="mega-post-date wcp-disable-post-meta">
		<i class="fa fa-clock-o"></i>
		<?php echo get_the_date() ?>
	</span>

	<div class="clearfix"></div>
	<div class="mega-post-para">
		<?php do_action( 'wcp_carousel_desc', $carousel_settings['desc'], get_the_id(), $carousel_settings['words']); ?>
        <?php if (isset($read_more_txt) && $read_more_txt != '') { ?>
            <a style="float:right;margin:10px;" href="<?php the_permalink(); ?>" target="<?php echo $read_more_target; ?>" class="<?php echo $read_more_classes; ?>"><?php echo $read_more_txt; ?>
            </a>
            <p style="clear:both;margin:0;padding:0;"></p>
        <?php } ?>		
	</div>
</div>