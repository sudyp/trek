<div class="mega-post-carousel4">
	<div class="mega-post-image">
		<a href="<?php the_permalink(); ?>">
			<?php do_action( 'wcp_carousel_thumb_url', get_the_id(), $images_size ); ?>
		</a>
	</div>

	<div class="mega-post-category">
		<?php $categories = get_the_category();
			$separator = ' , ';
			$output = '';
			if ( ! empty( $categories[0] ) ) {
			        $output .= '<a href="' . esc_url( get_category_link( $categories[0]->term_id ) ) . '">' . esc_html( $categories[0]->name ) . '</a>' . $separator;
			    echo trim( $output, $separator );
			}
		?>
	</div>
	<span class="mega-post-date">
		<i class="fa fa-clock-o"></i>
		<?php echo get_the_date() ?>
	</span>
	<h3 class="mega-post-title">
		<a href="<?php the_permalink(); ?>">
			<?php do_action( 'wcp_carousel_title', $carousel_settings['title'], get_the_id() ); ?>
		</a>
	</h3>
	<div class="clearfix"></div>
	<div class="mega-post-para">
		<?php do_action( 'wcp_carousel_desc', $carousel_settings['desc'], get_the_id(), $carousel_settings['words']); ?>
        <?php if (isset($read_more_txt) && $read_more_txt != '') { ?>
            <a style="float:right;margin:10px;" href="<?php the_permalink(); ?>" target="<?php echo $read_more_target; ?>" class="<?php echo $read_more_classes; ?>"><?php echo $read_more_txt; ?>
            </a>
            <p style="clear:both;margin:0;padding:0;"></p>
        <?php } ?>		
	</div>
	<span class="mega-post-meta wcp-disable-post-meta">
		<i class="fa fa-user"></i>
		<?php the_author_posts_link(); ?>
	</span>
	<span class="mega-comment-box wcp-disable-post-meta">
		<span class="mega-post-comment">
			<i class="fa fa-comment"></i>
			<?php
				$comments = wp_count_comments(get_the_id());
				echo $comments->total_comments;
			?>			
		</span>					
	</span>
	<div class="clearfix"></div>
</div>