<?php
/**
 * Plugin Name: Responsive Posts Carousel
 * Plugin URI: https://webcodingplace.com/responsive-posts-carousel-wordpress-plugin
 * Description: The best Posts Slider Plugin for WordPress you will ever need.
 * Version: 3.7.6
 * Author: WebCodingPlace
 * Author URI: http://webcodingplace.com/
 * License: GPLv2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wcp-carousel
 */

/*

  Copyright (C) 2015  Rameez  rameez.iqbal@live.com
*/
require_once('plugin.class.php');
require_once('carousel.class.php');

if( class_exists('WCP_Posts_Carousel')){
	
	$just_initialize = new WCP_Posts_Carousel;
}

if( class_exists('WCP_Responsive_Posts_Carousel')){
	
	$carousel_ob = new WCP_Responsive_Posts_Carousel;
}
?>